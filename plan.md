
# AWS S3 - Structure

- KCV_COURS
	+ GYM
		* NEW
		* DONE
	+ ABDOS
		* NEW
		* DONE
	+ BIKE
		* NEW
		* DONE
	+ TREK
		* NEW
		* DONE
	+ ELLIPTIK
		* NEW
		* DONE

# Etape 1 

- Trier les fichiers par type de borne ;
- Mettre les paires de fichiers (MP4 + PNG) sur S3 dans les dossiers NEW des dossiers correspondants ;
- Pour les cours qui sont GYM et ABDOS, mettre les fichiers uniquement dans GYM\NEW ;
	+ Ajouter dans ABDOS\NEW un fichier TXT vide qui a comme nom le numéro de la vidéo (par exemple : 512.txt).

Une fois que les paires de fichiers sont complétement téléchargés sur S3, passer à l'**étape 2**.

# Etape 2


